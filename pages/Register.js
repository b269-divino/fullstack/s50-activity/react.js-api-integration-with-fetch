import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

export default function Register() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    // to store values of the input fields
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

// useEffect(() => {
//     if((email !== "" && password1 !== "" && password2) && (password1 === password2)) {
//         setIsActive(true);
//     } else {
//         setIsActive(false);

//     }
// }, [email, password1, password2]);

    useEffect(() => {
    if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

        setIsActive(true)
    } else {
        setIsActive(false)
    }
}, [firstName, lastName, email, mobileNo, password1, password2])

    // function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading       
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {

                        // Clear input fields
                        setFirstName("");
                        setLastName("")
                        setEmail("");
                        setMobileNo("");
                        setPassword1("");
                        setPassword2("");

                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        })

                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }

                })
            }
        })

        // Clear input fields
        // setEmail("");
        // setPassword1("");
        // setPassword2("");

        // alert('Thank you for registering!');
    };

    return (
        (user.id !== null)?
        <Navigate to ="/products" />
        :
        <Form onSubmit={(e) => registerUser(e)} >

          <Form.Group className="mb-3" controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control 
                type="text"
                value={firstName}
                onChange={(e) => {setFirstName(e.target.value)}}
                placeholder="Enter your First Name" 
                required
                />
          </Form.Group>

          <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control 
                type="text"
                value={lastName}
                onChange={(e) => {setLastName(e.target.value)}}
                placeholder="Enter your Last Name" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email"
                value={email}
                onChange={(e) => {setEmail(e.target.value)}}
                placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control 
                type="text"
                value={mobileNo}
                onChange={(e) => {setMobileNo(e.target.value)}}
                placeholder="0999999999" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                value={password1}
                onChange={(e) => {setPassword1(e.target.value)}}
                placeholder="Enter Your Password" />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control 
                type="password" 
                value={password2}
                onChange={(e) => {setPassword2(e.target.value)}}
                placeholder="Verify Your Password" />
          </Form.Group>
          { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                     Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
          }
         
        </Form> 
    )

}








// // import {useState, useEffect} from 'react';

// import {useState, useEffect, useContext} from 'react';

// // import {useNavigate} from 'react-router-dom';
// import {Navigate} from 'react-router-dom';

// import UserContext from '../UserContext';

// import { Form, Button } from 'react-bootstrap';
// import Swal from 'sweetalert2';

// export default function Register() {

//     const [firstName, setFirstName] = useState('');
//     const [lastName, setLastName] = useState('');
//     const [mobileNumber, setMobileNumber] = useState('');

//     const [email, setEmail] = useState("");
//     const [password1, setPassword1] = useState("");
//     const [password2, setPassword2] = useState("");
//     // to determine whether submit button is enabled or not
//     const [isActive, setIsActive] = useState(false);
//     const {user} = useContext(UserContext);


//  function registerUser(e){
//         e.preventDefault()

//         fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({
//                 email: email,


//             })
//         })
//         .then(res => res.json())
//         .then(data => {
//             // We will receive either a token or an error response.
//             console.log(data);

//             // If no user information is found, the "access" property will not be available and will return undefined
//             // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

//             if (data === true) {
//                 // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
//                 // localStorage.setItem('token', data.access);
//                 // retrieveUserDetails(data.access);


//                 Swal.fire({

//                     title: "Duplicate Email Failed!",
//                     icon: "error",
//                     text: "Please check and try again!"

//                 })
//             } else {
// //                 Swal.fire({
// //                     title: "Login Successful!",
// //                     icon: "success",
// //                     text: "Welcome to Zuitt!"
// //                 })
// // };

// //     });

//         // Set the email of the authenticated user in the local storage
//         // Syntax
//         // localStorage.setItem('propertyName', value);
//         // localStorage.setItem('email', email);
//         // Sets the global user state to have properties obtain from local storage
//         // setUser({email: localStorage.getItem('email')});

//         // Clear input fields after submission

// //         setFirstName('');
// //         setLastName('');
// //         setMobileNumber('');
// //         setEmail("");
// //         setPassword1("");
// //         setPassword2("");
// //         // navigate('/')t

// //         // alert("Successfully login!")

        
// // };
// //     const retrieveUserDetails = (token) => {
// //             // The token will be sent as part of the request's header information
// //             // We put "Bearer" in front of the token to follow implementation standards for JWTs
// //             fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
// //                 headers: {
// //                     Authorization: `Bearer ${token}`
// //                 }
// //             })
// //             .then(res => res.json())
// //             .then(data => {
// //                 console.log(data);
// //                 // Global user state for validation accross the whole app
// //                 // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
// //                 setEmail({
// //                     id: data._id,
// //                     isAdmin: data.isAdmin
// //                 })
// //             })
// //         };


//     useEffect(() => {
//         if((

//             firstName !== "" && 
//             lastName !== "" && 
//             email !== "" && 
//             mobileNo.length === 11 &&  
//             password1 !== "" && password2) && 
//             password1 === password2) 
//         {
//         setIsActive(true);
//     } else {
//         setIsActive(false);
//         };
//     }, [

//         firstName, 
//         lastName,
//         email, 
//         mobileNo, 
//         email, 
//         password1, 
//         password2
        
//     ]);






//     return (
//         (user.id!== null) ?
//         <Navigate to="/login"/>
//         :
//         <Form onSubmit={(e) => registerUser(e)} >

//       <Form.Group controlId="userFirstName">
//         <Form.Label>First Name</Form.Label>
//         <Form.Control
//           type="text"
//           placeholder="Enter first name"
//           value={firstName}
//           onChange={(e) => setFirstName(e.target.value)}
//           required
//         />
//       </Form.Group>

//       <Form.Group controlId="userLastName">
//         <Form.Label>Last Name</Form.Label>
//         <Form.Control
//           type="text"
//           placeholder="Enter last name"
//           value={lastName}
//           onChange={(e) => setLastName(e.target.value)}
//           required
//         />
//       </Form.Group>

//       <Form.Group controlId="userMobileNumber">
//         <Form.Label>Mobile Number</Form.Label>
//         <Form.Control
//           type="tel"
//           placeholder="Enter 11 digit mobile number"
//           value={mobileNumber}
//           onChange={(e) => setMobileNumber(e.target.value)}
//           pattern="[0-9]{11,}"
//           required
//         />
//       </Form.Group>





//             <Form.Group controlId="userEmail">
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control 
//                     type="email" 
//                     placeholder="Enter email" 
//                     value={email}
//                     onChange={e => setEmail(e.target.value)}
//                     required
//                 />
//                 <Form.Text className="text-muted">
//                     We'll never share your email with anyone else.
//                 </Form.Text>
//             </Form.Group>

//             <Form.Group controlId="password1">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Password" 
//                     value={password1}
//                     onChange={e => setPassword1(e.target.value)}
//                     required
//                 />
//             </Form.Group>

//             <Form.Group controlId="password2">
//                 <Form.Label>Verify Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Verify Password" 
//                     value={password2}
//                     onChange={e => setPassword2(e.target.value)}
//                     required
//                 />
//             </Form.Group>

//             {isActive ?
//                 <Button variant="primary" type="submit" id="submitBtn">
//                 Submit
//                 </Button>
//                 :
//                 <Button variant="danger" type="submit" id="submitBtn" disabled>
//                 Submit
//                 </Button>
//             }
            
//         </Form>
//     )

// }
